FROM mongo
VOLUME [ "/data/db" ]
COPY init.js /docker-entrypoint-initdb.d/
EXPOSE 27017
CMD [ "mongod" ]